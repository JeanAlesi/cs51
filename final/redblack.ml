type color = Red | Black
type 'a btree = Node of color * 'a btree * 'a * 'a btree | Leaf

let rec mem x = function
Leaf -> false
| Node (_, a, y, b) ->
if x < y then mem x a
else if x > y then mem x b
else true

let balance = function
Black, Node (Red, Node (Red, a, x, b), y, c), z, d ->
Node (Red, Node (Black, a, x, b), y, Node (Black, c, z, d))
| Black, Node (Red, a, x, Node (Red, b, y, c)), z, d ->
Node (Red, Node (Black, a, x, b), y, Node (Black, c, z, d))
| Black, a, x, Node (Red, Node (Red, b, y, c), z, d) ->
Node (Red, Node (Black, a, x, b), y, Node (Black, c, z, d))
| Black, a, x, Node (Red, b, y, Node (Red, c, z, d)) ->
Node (Red, Node (Black, a, x, b), y, Node (Black, c, z, d))
| a, b, c, d ->
Node (a, b, c, d)

let insert x s =
let rec ins = function
Leaf -> Node (Red, Leaf, x, Leaf)
| Node (color, a, y, b) as s ->
if x < y then balance (color, ins a, y, b)
else if x > y then balance (color, a, y, ins b)
else s
in
match ins s with (* guaranteed to be non-empty *)
Node (_, a, y, b) -> Node (Black, a, y, b)
| Leaf -> raise (Invalid_argument "insert");;
