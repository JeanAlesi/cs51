(*

remove = 
  - key k is removed the same way as in BST.
  - Splay the parent of the deleted node up to the root.
  - If k not in tree, splay last node where search ended.




Check with TF if delete is correct!!!!!


 *)

open Core.Std

type order = EQUAL | LESS | GREATER

module type COMPARABLE =
sig
  type t
  val compare : t -> t -> order
  val to_string : t -> string
end

module IntCompare : COMPARABLE with type t = int =
struct
  type t = int

  let compare x y =
    if x < y then LESS else if x > y then GREATER else EQUAL
  let to_string = string_of_int

end


module type SPLAYTREE =
sig
   (* finish this before submitting *) 
end


module SplayTree(C : COMPARABLE) (* : SPLAYTREE *) =
struct  
  type elt = C.t
  type dir = LL | LR | RL | RR | L | R | N
  type task = SEARCH | INS | DEL
  type tree = Leaf | Branch of tree * elt * tree 
  type kicked = Splay of tree * elt * tree | Skip of tree * elt * tree 

  let empty : tree =  Leaf

  let isEmpty (t : tree) : bool = (t = Leaf)

  let branches_of (t : tree) : tree * elt * tree =
    match t with
    | Leaf -> failwith "No Branches"
    | Branch(l,v,r) -> (l,v,r)

  let rec pull_max (t : tree) : elt * tree =
    match t with
    | Leaf -> failwith "empty tree"
    | Branch(Leaf,v,Leaf) -> (v, Leaf)
    | Branch(l,v,Leaf) -> (v, l)
    | Branch(l,v,r) -> 
      let v', t' = pull_max r in (v', (Branch(l,v,t')))


  let splay ?(gp : tree option) (p : tree) (t : tree) (d : dir * dir) =
    let pl,pv,pr = branches_of p in
    let tl,tv,tr = branches_of t in
    match gp with
    | None ->
      (match snd d with
       | L -> Skip(tl,tv,Branch(tr,pv,pr))
       | R -> Skip(Branch(pl,pv,tl),tv,tr)
       | _ -> failwith "cannot zig in this direction")
    | Some Branch(gl,gv,gr) ->
      match d with
      | L,L -> Skip(tl,tv,Branch(tr,pv,Branch(pr,gv,gr)))
      | R,R -> Skip(Branch(Branch(gl,gv,pl),pv,tl),tv,tr)
      | L,R -> Skip(Branch(pl,pv,tl),tv,Branch(tr,gv,gr)) 
      | R,L -> Skip(Branch(gl,gv,tl),tv,Branch(tr,pv,pr))

      (* make sure the directions are correct *)

    


  let rec insert2 (g,g_dir) (p,p_dir) (t,t_dir) e =
    let gl,gv,gr = branches_of g in
    let pl,pv,pr = branches_of p in
    match t with
    | Leaf -> splay ~gp:g p (Branch(Leaf,e,Leaf)) (p_dir,t_dir) (* this line probably makes the difference between search and insert.. add if statement to toggle between insert and search *)
    | Branch(l,v,r) -> 
      (match C.compare e v with
       | EQUAL -> splay ~gp:g p t (p_dir,t_dir)
       | LESS -> 
          let temp = insert2 (p,p_dir) (t,t_dir) (l,L) e in
          (match temp with
	   | Splay(l',v',r') -> splay ~gp:g p (Branch(l',v',r')) (t_dir,L)
	   | Skip(l',v',r') -> 
            if g_dir = N then splay g (Branch(l',v',r')) (N,p_dir)
	    else Splay(l',v',r'))
       | GREATER ->
          let temp = insert2 (p,p_dir) (t,t_dir) (r,R) e in
          (match temp with
	   | Splay(l',v',r') -> splay ~gp:g p (Branch(l',v',r')) (t_dir,R)
	   | Skip(l',v',r') ->
             if g_dir = N then splay g (Branch(l',v',r')) (N,p_dir)
	     else Splay(l',v',r')))
	  



  let rec insert (e : elt) (t : tree) : tree =
    match t with
    | Leaf -> Branch(Leaf,e,Leaf)
    | Branch(l,v,r) ->
      (match C.compare e v with
       | EQUAL -> t (* account for repetitions *)
       | LESS ->
         (match l with
	  | Leaf -> Branch(Leaf,e,t)
	  | Branch(ll,lv,lr) ->
            (match C.compare e lv with
             | EQUAL -> Branch(ll,lv,Branch(lr,v,r))
	     | LESS -> 
               (match insert2 (t,N) (l,L) (ll,L) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))   (* decide if splay or skip... or both *)
	     | GREATER -> 
               (match insert2 (t,N) (l,L) (lr,R) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))))
         | GREATER ->
         (match r with
	  | Leaf -> Branch(t,e,Leaf)
	  | Branch(rl,rv,rr) ->
            (match C.compare e rv with
	     | EQUAL -> Branch(Branch(l,v,rl),rv,rr)
	     | LESS -> 
               (match insert2 (t,N) (r,R) (rl,L) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))
	     | GREATER -> 
               (match insert2 (t,N) (r,R) (rr,R) e with
	        | Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r)))))
  



(* for testing only *)
let draw tree =
  let rec print indent tree =
    match tree with
       Leaf -> 
        Printf.printf "Leaf\n"
     | Branch(left, n, right) ->
        Printf.printf "%s----\n" indent;
        print (indent ^ "| ") left;
        print_string (indent ^ "     " ^ (C.to_string n) ^ "\n");
        print (indent ^ "| ") right;
        Printf.printf "%s----\n" indent
  in
  print "" tree




(* ------------------------------------------------------------ *)
(* ------------------------------------------------------------ *)
(* ---------------------------------------------------------- *)


  let rec process_task (tsk : task) (g,g_dir) (p,p_dir) (t,t_dir) e =
    let gl,gv,gr = branches_of g in
    let pl,pv,pr = branches_of p in
    match t with
    | Leaf -> 
      (match tsk with
       | INS -> splay ~gp:g p (Branch(Leaf,e,Leaf)) (p_dir,t_dir)
       | SEARCH -> Splay(pl,pv,pr))  (* node wasn't found, make last node visited the root *)
    | Branch(l,v,r) -> 
      (match C.compare e v with
       | EQUAL -> 
         (match tsk with
	  | INS | SEARCH -> splay ~gp:g p t (p_dir,t_dir))
       | LESS -> 
          let temp = process_task tsk (p,p_dir) (t,t_dir) (l,L) e in
          (match temp with
	   | Splay(l',v',r') -> splay ~gp:g p (Branch(l',v',r')) (t_dir,L)    (* Acordarse de a;adir el caso de que sea Splay en level. check for N in Splay(....) case. 2 *)
	   | Skip(l',v',r') -> 
            if g_dir = N then splay g (Branch(l',v',r')) (N,p_dir)
	    else Splay(l',v',r'))
       | GREATER ->
          let temp = process_task tsk (p,p_dir) (t,t_dir) (r,R) e in
          (match temp with
	   | Splay(l',v',r') -> splay ~gp:g p (Branch(l',v',r')) (t_dir,R)
	   | Skip(l',v',r') ->
             if g_dir = N then splay g (Branch(l',v',r')) (N,p_dir)
	     else Splay(l',v',r')))






  let start_task (tsk : task) (e : elt) (t : tree) : tree =
    match t with
    | Leaf -> 
      (match tsk with
       | INS -> Branch(Leaf,e,Leaf)
       | SEARCH -> t)
    | Branch(l,v,r) ->
      (match C.compare e v with
       | EQUAL ->
         (match tsk with
	  | INS | SEARCH -> t)
       | LESS ->
         (match l with
	  | Leaf -> 
            (match tsk with
	     | INS -> Branch(Leaf,e,t)
	     | SEARCH -> t)
	  | Branch(ll,lv,lr) ->
            (match C.compare e lv with
             | EQUAL -> 
               (match tsk with
		| INS | SEARCH -> Branch(ll,lv,Branch(lr,v,r)))
	     | LESS -> 
               (match process_task tsk (t,N) (l,L) (ll,L) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))   (* decide if splay or skip... or both *)
	     | GREATER -> 
               (match process_task tsk (t,N) (l,L) (lr,R) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))))
         | GREATER ->
         (match r with
	  | Leaf ->
            (match tsk with
	     | INS -> Branch(t,e,Leaf)
	     | SEARCH -> t)
	  | Branch(rl,rv,rr) ->
            (match C.compare e rv with
	     | EQUAL -> 
               (match tsk with
		| INS | SEARCH -> Branch(Branch(l,v,rl),rv,rr))
	     | LESS -> 
               (match process_task tsk (t,N) (r,R) (rl,L) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))
	     | GREATER -> 
               (match process_task tsk (t,N) (r,R) (rr,R) e with
	        | Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r)))))


 (* ---------------------------------------------------------- *)
 (* ---------------------------------------------------------- *)
 (* ---------------------------------------------------------- *)


  let insert (e : elt) (t : tree) = start_task INS e t

  let search (e : elt) (t : tree) = start_task SEARCH e t

  (* same as web page *)
  let delete (e : elt) (t : tree) =
    match start_task SEARCH e t with
    | Leaf -> Leaf
    | Branch(l,v,r) ->
      let v', l' = pull_max l in
      Branch(l',v',r)

end


module S = SplayTree(IntCompare);;

let t = S.Branch (S.Branch (S.Branch (S.Leaf, 7, S.Leaf), 8, S.Leaf), 10, S.Leaf)
let _  = assert( (S.insert 6 t) = (S.Branch (S.Leaf, 6, S.Branch (S.Branch (S.Leaf, 7, S.Branch (S.Leaf, 8, S.Leaf)), 10, S.Leaf))) );;
let t1 = S.Branch (S.Branch (S.Leaf, 9, S.Leaf), 11, S.Leaf);;
let t2 = S.Branch (S.Leaf, 9, S.Leaf);;
let t3 = S.Branch (S.Leaf, 3, S.Branch (S.Leaf, 42, S.Branch (S.Leaf, 46, S.Leaf)));;
let t4 = S.Branch (S.Leaf, 42, S.Branch (S.Leaf, 46, S.Leaf));;
let t5 = S.Branch (S.Leaf, 46, S.Leaf);;
let _ = assert( S.search 11 t = S.Branch (S.Branch (S.Branch (S.Leaf, 7, S.Leaf), 8, S.Leaf), 10, S.Leaf));;
;;



