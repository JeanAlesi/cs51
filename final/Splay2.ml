(*

a) find object
   begin like an ordinary BST. walk down tree to entry with key k or dead end.
   Let x be the node where the search ended, whether it contains k or not.
   Splay x up the tree througha sequence of rotations, so X becomes the root.
   
  3 cases:
1) X is left child of a right child OR right child of left child.
2) X is left child of left child OR right child of right child

Repeat 1 & 2

3) X is child of root


functions:
insert =
  - insert new entry (same as in ordinary binary trees)
  - Splay new node to the root

remove = 
  - key k is removed the same way as in BST.
  - Splay the parent of the deleted node up to the root.
  - If k not in tree, splay last node where search ended.

 *)

open Core.Std

type order = EQUAL | LESS | GREATER

module type COMPARABLE =
sig
  type t
  val compare : t -> t -> order
  val to_string : t -> string
end

module IntCompare : COMPARABLE with type t = int =
struct
  type t = int

  let compare x y =
    if x < y then LESS else if x > y then GREATER else EQUAL
  let to_string = string_of_int

end


module type SPLAYTREE =
sig
   (* finish this before submitting *) 
end


module SplayTree(C : COMPARABLE) (* : SPLAYTREE *) =
struct 

 
  type elt = C.t
  type dir = LL | LR | RL | RR | L | R | N
  type tree = Leaf | Branch of tree * elt * tree 
  type kicked = Splay of tree * elt * tree | Skip of tree * elt * tree 

  let empty : tree =  Leaf

  let isEmpty (t : tree) : bool = (t = Leaf)

  let branches_of (t : tree) : tree * elt * tree =
    match t with
    | Leaf -> failwith "No Branches"
    | Branch(l,v,r) -> (l,v,r)

  let splay ?(gp : tree option) (p : tree) (t : tree) (d : dir * dir) =
    let pl,pv,pr = branches_of p in
    let tl,tv,tr = branches_of t in
    match gp with
    | None ->
      (match snd d with
       | L -> Skip(tl,tv,Branch(tr,pv,pr))
       | R -> Skip(Branch(pl,pv,tl),tv,tr)
       | _ -> failwith "cannot zig in this direction")
    | Some Branch(gl,gv,gr) ->
      match d with
      | L,L -> Skip(tl,tv,Branch(tr,pv,Branch(pr,gv,gr)))
      | R,R -> Skip(Branch(Branch(gl,gv,pl),pv,tl),tv,tr)
      | L,R -> Skip(Branch(pl,pv,tl),tv,Branch(tr,gv,gr)) 
      | R,L -> Skip(Branch(gl,gv,tl),tv,Branch(tr,pv,pr))

      (* make sure the directions are correct *)
   

  (* change in EQUAL for deletion *)
  let rec insert2 (g,g_dir) (p,p_dir) (t,t_dir) e =
    let gl,gv,gr = branches_of g in
    let pl,pv,pr = branches_of p in
    match t with
    | Leaf -> 
      Splay(pl,pv,pr)  (* for delete, same as search at leaf *)
(* splay ~gp:g p (Branch(Leaf,e,Leaf)) (p_dir,t_dir) *) (* this line probably makes the difference between search and insert.. add if statement to toggle between insert and search *)
    | Branch(l,v,r) -> 
      (match C.compare e v with
       | EQUAL -> Splay(pl,pv,pr) (* splay ~gp:g p t (p_dir,t_dir)     *)
       | LESS -> 
          let temp = insert2 (p,p_dir) (t,t_dir) (l,L) e in
          (match temp with
	   | Splay(l',v',r') -> splay ~gp:g p (Branch(l',v',r')) (t_dir,L)  (* make sure, also check N in Splay *)
	   | Skip(l',v',r') -> 
            if g_dir = N then splay g (Branch(l',v',r')) (N,p_dir)
	    else Splay(l',v',r'))
       | GREATER ->
          let temp = insert2 (p,p_dir) (t,t_dir) (r,R) e in
          (match temp with
	   | Splay(l',v',r') -> splay ~gp:g p (Branch(l',v',r')) (t_dir,R)
	   | Skip(l',v',r') ->
             if g_dir = N then splay g (Branch(l',v',r')) (N,p_dir)
	     else Splay(l',v',r')))
	  



  let rec insert (e : elt) (t : tree) : tree =
    match t with
    | Leaf -> Branch(Leaf,e,Leaf)
    | Branch(l,v,r) ->
      (match C.compare e v with
       | EQUAL -> Leaf(* t  *) (* account for repetitions *)
       | LESS ->
         (match l with
	  | Leaf -> Branch(Leaf,e,t)
	  | Branch(ll,lv,lr) ->
            (match C.compare e lv with
             | EQUAL -> Branch(Leaf,v,r)   (* Branch(ll,lv,Branch(lr,v,r)) *)
	     | LESS -> 
               (match insert2 (t,N) (l,L) (ll,L) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))   (* decide if splay or skip... or both *)
	     | GREATER -> 
               (match insert2 (t,N) (l,L) (lr,R) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))))
         | GREATER ->
         (match r with
	  | Leaf -> Branch(t,e,Leaf)
	  | Branch(rl,rv,rr) ->
            (match C.compare e rv with
	     | EQUAL -> Branch(Branch(l,v,rl),rv,rr)
	     | LESS -> 
               (match insert2 (t,N) (r,R) (rl,L) e with
		| Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r))
	     | GREATER -> 
               (match insert2 (t,N) (r,R) (rr,R) e with
	        | Splay(l,v,r) | Skip(l,v,r) -> Branch(l,v,r)))))


(* for testing only *)
let draw tree =
  let rec print indent tree =
    match tree with
       Leaf -> 
        Printf.printf "Leaf\n"
     | Branch(left, n, right) ->
        Printf.printf "%s----\n" indent;
        print (indent ^ "| ") left;
        print_string (indent ^ "     " ^ (C.to_string n) ^ "\n");
        print (indent ^ "| ") right;
        Printf.printf "%s----\n" indent
  in
  print "" tree


end

