open Core.Std
open Rbt

module RB = RBT (IntCompare)

let rec list_insert (l : int list) (t : RB.tree) : RB.tree =
match l with
|[] -> t
|hd :: tl ->  list_insert tl (RB.insert hd t)


let rec generate_list (n : int) : int list =
match n with
| 0 -> []
| x -> x :: generate_list (n  -1)

let rec delete_list (l: int list) (t: RB.tree) : RB.tree =
match l with
| [] -> t
| hd :: tl -> delete_list tl (RB.delete hd t)

let l = generate_list 10
let test_tree = list_insert l RB.empty


let t = Unix.gettimeofday () in
let _ = RB.insert 10001 test_tree in
let time = Unix.gettimeofday () -. t in
time

